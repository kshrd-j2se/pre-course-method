package org.kshrd.a;

public class Student {

    private int id;
    private String name;
    private String gender;
    private String province;
    private double score;
    private String currentAdress;

    public Student(int id, String name, String gender, String province, double score) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.province = province;
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public double getScore() {
        if (province == "Kandal") {
            return score + 30;
        }
        return score;
    }

    public void setScore(double score) {
        if (score < 0 || score > 100)
            return;
        this.score = score;
    }
}
