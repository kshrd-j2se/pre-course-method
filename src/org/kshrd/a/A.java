package org.kshrd.a;

public class A {

    private int aPrivateVar = 1;
    double aDefaultVar = 5;
    protected String aProtectedVar = "Hello";
    public String aPublicVar = "Sou Sdey";

    void method() {
        aPrivateVar = 3;
    }

    public static void main(String[] args) {
        System.out.print(new A().aPrivateVar);

    }

}
